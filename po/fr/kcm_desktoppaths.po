# translation of kcm_desktoppaths.po to Français
# translation of kcmkonq.po to
# traduction de kcmkonq.po en Français
# Copyright (C) 2002,2003, 2004, 2005, 2006, 2008, 2010 Free Software Foundation, Inc.
# Gérard Delafond <gerard@delafond.org>, 2002, 2004.
# Gilles Caulier <caulier.gilles@free.fr>, 2002,2003.
# Matthieu Robin <kde@macolu.org>, 2003, 2004.
# Matthieu Robin <kde@macolu.org>, 2004, 2005, 2006.
# Nicolas Ternisien <nicolas.ternisien@gmail.com>, 2004, 2005.
# Nicolas Ternisien <nicolas.ternisien@gmail.com>, 2005, 2008, 2010.
# Sébastien Renard <Sebastien.Renard@digitalfox.org>, 2008.
# Mickael Sibelle <kimael@gmail.com>, 2008.
# Joëlle Cornavin <jcornavin@laposte.net>, 2010.
# xavier <ktranslator31@yahoo.fr>, 2013.
# Vincent Pinon <vpinon@kde.org>, 2017.
# Xavier Besnard <xavier.besnard@neuf.fr>, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kcm_desktoppaths\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-06 01:02+0000\n"
"PO-Revision-Date: 2022-12-04 17:10+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.11.90\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: desktoppathssettings.cpp:211
#, kde-format
msgid "Desktop"
msgstr "Bureau"

#: desktoppathssettings.cpp:226
#, kde-format
msgid "Documents"
msgstr "Documents"

#: desktoppathssettings.cpp:241
#, kde-format
msgid "Downloads"
msgstr "Téléchargements"

#: desktoppathssettings.cpp:256
#, kde-format
msgid "Music"
msgstr "Musique"

#: desktoppathssettings.cpp:271
#, kde-format
msgid "Pictures"
msgstr "Photos"

#: desktoppathssettings.cpp:286
#, kde-format
msgid "Videos"
msgstr "Vidéos"

#: desktoppathssettings.cpp:301
#, kde-format
msgid "Public"
msgstr "Publique"

#: desktoppathssettings.cpp:316
#, kde-format
msgid "Templates"
msgstr "Modèles"

#: globalpaths.cpp:27
#, kde-format
msgid ""
"<h1>Paths</h1>\n"
"This module allows you to choose where in the filesystem the files on your "
"desktop should be stored.\n"
"Use the \"Whats This?\" (Shift+F1) to get help on specific options."
msgstr ""
"<h1>Emplacements</h1>\n"
"Ce module permet de choisir où sont enregistrés les fichiers présents sur "
"votre bureau, dans le système de fichiers.\n"
"Veuillez consulter l'aide « Qu'est-ce que c'est ? » (Maj+F1) pour plus "
"d'informations concernant les options spécifiques."

#: package/contents/ui/main.qml:22
#, kde-format
msgid "Desktop path:"
msgstr "Dossier « Bureau » :"

#: package/contents/ui/main.qml:25
#, kde-format
msgid ""
"This folder contains all the files which you see on your desktop. You can "
"change the location of this folder if you want to, and the contents will "
"move automatically to the new location as well."
msgstr ""
"Ce dossier contient tous les fichiers que vous voyez sur votre bureau. Vous "
"pouvez changer l'emplacement de ce dossier si vous le souhaitez et son "
"contenu sera automatiquement déplacé vers son nouvel emplacement également."

#: package/contents/ui/main.qml:31
#, kde-format
msgid "Documents path:"
msgstr "Dossier « Documents » :"

#: package/contents/ui/main.qml:34
#, kde-format
msgid ""
"This folder will be used by default to load or save documents from or to."
msgstr ""
"Ce dossier sera utilisé par défaut pour charger ou enregistrer les documents."

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Downloads path:"
msgstr "Dossier « Téléchargements » :"

#: package/contents/ui/main.qml:43
#, kde-format
msgid "This folder will be used by default to save your downloaded items."
msgstr ""
"Ce dossier sera utilisé par défaut pour enregistrer vos documents "
"téléchargés."

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Videos path:"
msgstr "Emplacement des vidéos :"

#: package/contents/ui/main.qml:52 package/contents/ui/main.qml:79
#, kde-format
msgid "This folder will be used by default to load or save movies from or to."
msgstr ""
"Ce dossier sera utilisé par défaut pour charger ou enregistrer des films."

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Pictures path:"
msgstr "Dossier « Photos » :"

#: package/contents/ui/main.qml:61
#, kde-format
msgid ""
"This folder will be used by default to load or save pictures from or to."
msgstr ""
"Ce dossier sera utilisé par défaut pour charger ou enregistrer des photos."

#: package/contents/ui/main.qml:67
#, kde-format
msgid "Music path:"
msgstr "Dossier « Musique » :"

#: package/contents/ui/main.qml:70
#, kde-format
msgid "This folder will be used by default to load or save music from or to."
msgstr ""
"Ce dossier sera utilisé par défaut pour charger ou enregistrer de la musique."

#: package/contents/ui/main.qml:76
#, kde-format
msgid "Public path:"
msgstr "Emplacement publique :"

#: package/contents/ui/main.qml:85
#, kde-format
msgid "Templates path:"
msgstr "Emplacement des modèles :"

#: package/contents/ui/main.qml:88
#, kde-format
msgid ""
"This folder will be used by default to load or save templates from or to."
msgstr ""
"Ce dossier sera utilisé par défaut pour charger ou enregistrer des modèles, "
"respectivement à partir de ou vers."

#: package/contents/ui/UrlRequester.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Choose new location"
msgstr ""

#~ msgid ""
#~ "This folder will be used by default to load or save public shares from or "
#~ "to."
#~ msgstr ""
#~ "Ce dossier sera utilisé par défaut pour charger ou enregistrer des "
#~ "partages publiques, respectivement à partir de ou vers."

#~ msgid "Autostart path:"
#~ msgstr "Dossier « Démarrage automatique » :"

#~ msgid ""
#~ "This folder contains applications or links to applications (shortcuts) "
#~ "that you want to have started automatically whenever the session starts. "
#~ "You can change the location of this folder if you want to, and the "
#~ "contents will move automatically to the new location as well."
#~ msgstr ""
#~ "Ce dossier contient des applications ou des liens vers des applications "
#~ "(raccourcis) que vous voulez voir démarrées automatiquement à chaque "
#~ "lancement de la session. Vous pouvez changer l'emplacement de ce dossier "
#~ "si vous le souhaitez et son contenu sera automatiquement déplacé vers son "
#~ "nouvel emplacement également."

#~ msgid "Autostart"
#~ msgstr "Démarrage automatique"

#~ msgid "Movies"
#~ msgstr "Films"

#~ msgid ""
#~ "The path for '%1' has been changed.\n"
#~ "Do you want the files to be moved from '%2' to '%3'?"
#~ msgstr ""
#~ "L'emplacement de « %1 » a été modifié.\n"
#~ "Voulez-vous que les fichiers de « %2 » soient déplacés dans « %3 » ?"

#~ msgctxt "Move files from old to new place"
#~ msgid "Move"
#~ msgstr "Déplacer"

#~ msgctxt "Use the new directory but do not move files"
#~ msgid "Do not Move"
#~ msgstr "Ne pas déplacer"

#~ msgid ""
#~ "The path for '%1' has been changed.\n"
#~ "Do you want to move the directory '%2' to '%3'?"
#~ msgstr ""
#~ "L'emplacement de « %1 » a été modifié.\n"
#~ "Voulez-vous déplacer le dossier « %2 » dans « %3 » ?"

#~ msgctxt "Move the directory"
#~ msgid "Move"
#~ msgstr "Déplacer"

#~ msgctxt "Use the new directory but do not move anything"
#~ msgid "Do not Move"
#~ msgstr "Ne pas déplacer"

#~ msgid "Confirmation Required"
#~ msgstr "Confirmation demandée"
