# Translation of kcm_baloofile.po to Catalan (Valencian)
# Copyright (C) 2014-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2014, 2017, 2019, 2020, 2021, 2022.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019, 2020, 2021.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-26 00:59+0000\n"
"PO-Revision-Date: 2022-12-14 09:02+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#: package/contents/ui/main.qml:21
#, kde-format
msgid ""
"This module lets you configure the file indexer and search functionality."
msgstr ""
"Este mòdul permet configurar la funcionalitat de l'indexador i busca de "
"fitxers."

#: package/contents/ui/main.qml:58
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"Desactivarà la busca de fitxers en KRunner i en els menús de l'iniciador, i "
"eliminarà la visualització de metadades ampliades en totes les aplicacions "
"KDE."

#: package/contents/ui/main.qml:67
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Voleu suprimir les dades guardades d'índex? S'alliberarà %1 d'espai, però si "
"més tard es reactiva la indexació, es tornarà a crear l'índex sencer des de "
"zero. Açò pot tardar una estona, depenent de la quantitat de fitxers que hi "
"haja."

#: package/contents/ui/main.qml:69
#, kde-format
msgid "Delete Index Data"
msgstr "Suprimix les dades d'índex"

#: package/contents/ui/main.qml:89
#, kde-format
msgid "The system must be restarted before these changes will take effect."
msgstr "Cal reiniciar el sistema perquè estos canvis tinguen efecte."

#: package/contents/ui/main.qml:93
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Reinicia"

#: package/contents/ui/main.qml:100
#, kde-format
msgid ""
"File Search helps you quickly locate all your files based on their content."
msgstr ""
"La busca de fitxers us ajuda a localitzar ràpidament tots els fitxers basant-"
"se en el seu contingut."

#: package/contents/ui/main.qml:107
#, kde-format
msgid "Enable File Search"
msgstr "Activa la busca de fitxers"

#: package/contents/ui/main.qml:122
#, kde-format
msgid "Also index file content"
msgstr "Indexa també el contingut dels fitxers"

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Index hidden files and folders"
msgstr "Indexa els fitxers i carpetes ocults"

#: package/contents/ui/main.qml:160
#, kde-format
msgid "Status: %1, %2% complete"
msgstr "Estat: %1, %2% complet"

#: package/contents/ui/main.qml:165
#, kde-format
msgid "Pause Indexer"
msgstr "Posa en pausa l'indexador"

#: package/contents/ui/main.qml:165
#, kde-format
msgid "Resume Indexer"
msgstr "Reprén l'indexador"

#: package/contents/ui/main.qml:178
#, kde-format
msgid "Currently indexing: %1"
msgstr "Actualment s'està indexant: %1"

#: package/contents/ui/main.qml:183
#, kde-format
msgid "Folder specific configuration:"
msgstr "Configuració específica de carpeta:"

#: package/contents/ui/main.qml:210
#, kde-format
msgid "Start indexing a folder…"
msgstr "Inicia la indexació d'una carpeta…"

#: package/contents/ui/main.qml:221
#, kde-format
msgid "Stop indexing a folder…"
msgstr "Para la indexació d'una carpeta…"

#: package/contents/ui/main.qml:272
#, kde-format
msgid "Not indexed"
msgstr "No indexada"

#: package/contents/ui/main.qml:273
#, kde-format
msgid "Indexed"
msgstr "Indexada"

#: package/contents/ui/main.qml:303
#, kde-format
msgid "Delete entry"
msgstr "Suprimix l'entrada"

#: package/contents/ui/main.qml:318
#, kde-format
msgid "Select a folder to include"
msgstr "Seleccioneu una carpeta que s'haja d'incloure"

#: package/contents/ui/main.qml:318
#, kde-format
msgid "Select a folder to exclude"
msgstr "Seleccioneu una carpeta que calga excloure"
