# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vincenzo Reale <smart2128vr@gmail.com>, 2014, 2019, 2020, 2021, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-06 00:49+0000\n"
"PO-Revision-Date: 2023-01-07 12:50+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.1\n"

#: contents/ui/ToolBoxContent.qml:266
#, kde-format
msgid "Choose Global Theme…"
msgstr "Scegli il tema globale..."

#: contents/ui/ToolBoxContent.qml:273
#, kde-format
msgid "Configure Display Settings…"
msgstr "Configura le impostazioni dello schermo…"

#: contents/ui/ToolBoxContent.qml:294
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr "Altro"

#: contents/ui/ToolBoxContent.qml:309
#, kde-format
msgid "Exit Edit Mode"
msgstr "Esci dalla modalità di modifica"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Completamento personalizzazione della disposizione"

#~ msgid "Default"
#~ msgstr "Predefinito"

#~ msgid "Desktop Toolbox"
#~ msgstr "Strumenti del desktop"

#~ msgid "Desktop Toolbox — %1 Activity"
#~ msgstr "Strumenti del desktop — %1 attività"

#~ msgid "Lock Screen"
#~ msgstr "Blocca schermo"

#~ msgid "Leave"
#~ msgstr "Esci"
