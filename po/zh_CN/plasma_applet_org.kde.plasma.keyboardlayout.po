msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-06 01:02+0000\n"
"PO-Revision-Date: 2023-01-12 10:31\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/plasma-desktop/plasma_applet_org.kde."
"plasma.keyboardlayout.pot\n"
"X-Crowdin-File-ID: 25212\n"

#: contents/config/config.qml:6
#, kde-format
msgid "General"
msgstr "常规"

#: contents/ui/configGeneral.qml:24
#, kde-format
msgid "Display style:"
msgstr "显示风格："

#: contents/ui/configGeneral.qml:64
#, kde-format
msgctxt "@info:placeholder Make this translation as short as possible"
msgid "No flag available"
msgstr "无可用旗帜"

#: contents/ui/configGeneral.qml:105
#, kde-format
msgid "Layouts:"
msgstr "布局："

#: contents/ui/configGeneral.qml:106
#, kde-format
msgid "Configure…"
msgstr "配置…"
